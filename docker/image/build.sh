#!/bin/sh
set -eu

# Compiler configuration

# Sets the DC and DVER environment variables based on the DMD environment
# variable, if present. The DMD variable is expected to hold the DMD version to
# use:
# For DMD 1.x, DC will be set to dmd1 and DVER to 1.
# For DMD 2.x.y.sN, DC will be set to dmd-transitional and DVER to 2.
# For DMD 2.x.y, DC will be set to dmd and DVER to 2.
# It errors if DMD is not set.
set_dc_dver() {
    old_opts=$-
    set -eu

    # Binary name deduced based on supplied DMD version
    case "$DMD" in
        dmd*   ) DC="$DMD"
                DVER=2
                if test "$DMD" = dmd1
                then
                    DVER=1
                fi
                ;;
        1.*    ) DC=dmd1 DVER=1 ;;
        2.*.s* ) DC=dmd-transitional DVER=2 ;;
        2.*    ) DC=dmd DVER=2 ;;
        *      ) echo "Unknown \$DMD ($DMD)" >&2; false ;;
    esac

    D2_ONLY=false
    if test -r ".D2-ready" && grep -q "^ONLY$" ".D2-ready"
    then
        D2_ONLY=true
    fi

    export DC DVER D2_ONLY
    set -$old_opts
}

set_dc_dver

# Install dependencies

case "$DMD" in
    dmd*   ) PKG= ;;
    1.*    ) PKG="dmd1=$DMD-$DIST" ;;
    2.*.s* ) PKG="dmd-transitional=$DMD-$DIST" ;;
    2.*    ) if [ $(echo $DMD | cut -d. -f2) -ge 077 ]; then
                PKG="dmd-compiler=$DMD dmd-tools=$DMD libphobos2-dev=$DMD"
             else
                PKG="dmd-bin=$DMD libphobos2-dev=$DMD"
             fi ;;
    *      ) echo "Unknown \$DMD ($DMD)" >&2; exit 1 ;;
esac

apt update
apt install -y --allow-downgrades \
    $PKG \
    libebtree6-dev \
    libtokyocabinet-dev \
    liblzo2-dev \
    libglib2.0-dev \
    libpcre3-dev \
    libgcrypt-dev \
    libgpg-error-dev

# Build app

make all pkg F=production
